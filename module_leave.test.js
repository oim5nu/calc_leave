const { calculateLeave } = require('./module_leave');
const fs = require('fs');
const mockDataFile = 'mockDataFile.txt';

function createMockDataFile() {
  return new Promise((resolve, reject) => {
    function onFinish() {
      resolve(content);
    }

    function onError(err) {
      reject(err);
    }
    
    const content = 
`Ben 20180320 20180327
Kieran 20180519 20180630
Ben 20180621 20180621
Abiel 20170122 20170211 // This is another commment
Tom 20160221 201602  // invalid date would be ignored
jason 20180314 20180316
// This is a comment and should be ignored.`;

    let writeStream = fs.createWriteStream(mockDataFile);
    
    // write some data with a base64 encoding
    writeStream.write(content);
    

    // the finish event is emitted when all data has been flushed from the stream
    writeStream.on('finish', onFinish);
    writeStream.on('error', onError);
    
    // close the stream
    writeStream.end();
  });    
}

function removeMockDataFile() {
  return new Promise((resolve, reject) => {
    fs.unlink(mockDataFile, (err) => {
      if(err) reject(err);
      resolve();
    });
    resolve();
  });
}

describe('Module Leave', ()=> {
  beforeEach(() => {
    createMockDataFile()
      .then(() => console.log('Mock data file created Successfully.'))
      .catch(err => console.log('Error occured in creating Mock data file', err) );
  });
  
  afterEach(() => {
    removeMockDataFile()
      .then(() => console.log('Mock data file removed Successfully.'))
      .catch(err => console.log('Error occured in removing Mock data file', err) );
  });

  test('calculateLeave', () => {
    return calculateLeave(mockDataFile)
      .then(received => {
        expect.assertions(1);
        const expected = [ 
          { name: 'Abiel', total: 21 },
          { name: 'Ben', total: 9 },
          { name: 'jason', total: 3 },
          { name: 'Kieran', total: 43 } ];
        expect(received).toEqual(expected);
      });
  });

  // may add more failure catch test if needed
});


