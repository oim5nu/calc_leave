const fs = require('fs');
const readline = require('readline');
const stream = require('stream');

function yyyymmddToDate(yyyymmdd) {
  let date = NaN;
  if ( /\D+/.test(yyyymmdd) === false && typeof yyyymmdd  === 'string' ) { // Digit Only
    if ( yyyymmdd.length == 8 ) {
      year = parseInt(yyyymmdd.slice(0, 4), 10);
      month = parseInt(yyyymmdd.slice(4, 6), 10);
      day = parseInt(yyyymmdd.slice(6, 8), 10);
      date = new Date(year, month - 1, day);
    }
  }
  return date;
}

function isEmpty(value) {
  return value === undefined || value === null || value === '';
}

function daysBetween(date1, date2) { // must be instanceof Date === true here
  //Get 1 day in milliseconds
  var one_day = 1000*60*60*24;

  // Convert both dates to milliseconds
  var date1_ms = date1.getTime();
  var date2_ms = date2.getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;
    
  // Convert back to days and return, 
  // may use Math.floor if date1, date1 is accurate timestamp
  return Math.round(difference_ms/one_day);  
}

function calculateLeave(filePath) {
  return new Promise((resolve, reject) => { 
    function onError(err){
      reject(err);
    }

    function onReadLine(line) {
      // remove comments
      if (line.indexOf('//') !== -1) { 
        line = line.split('//')[0]; 
      }
      let name= line.split(' ')[0];
      let startDateStr = line.split(' ')[1];
      let endDateStr = line.split(' ')[2];

      // convert to javascript timestamp
      let startDate = yyyymmddToDate(startDateStr);
      let endDate = yyyymmddToDate(endDateStr);
 
      //console.log('startDate', startDate);
      //console.log('endDate', endDate);
      //console.log('isNaN(endDate) === false', isNaN(endDate) === false);
      if ( isEmpty(name) === false && 
           isNaN(startDate) === false && 
           isNaN(endDate) === false ) {
        let total = daysBetween(startDate, endDate) + 1;
        let index = employeeLeaves.findIndex(obj => obj.name === name );
        if ( index !== -1 ) { // found it, adding it up
          employeeLeaves[index].total = employeeLeaves[index].total + total;
        } else { // append 
          employeeLeaves.push({ name, total });
        } 
      }
    }    

    function onClose() {
      employeeLeaves.sort((a, b) => a.name.localeCompare(b.name));  
      resolve(employeeLeaves);
    }

    let instream = fs.createReadStream(filePath);
    let outstream = new stream();
    let rl = readline.createInterface(instream, outstream);

    let employeeLeaves = []; 

    rl.on('line', onReadLine);
    rl.on('error', onError);
    rl.on('close', onClose);
  });
}

module.exports = { calculateLeave };
