
# Calc_Leave

Code snippet to sum up emloyee leave from text file. 

Please refer to the features of coding.challenge.ubank.pdf in root folder

## Concerns and Assumptions
- The code nippet is NOT for production purpose.
- Suggested Running Environment:  node.js > 4.0.0, i.e. natively supported promise features 
- For simplicity, the dates have not been undertaken validation checks
- Big data files have not been tested if concerned

## Installation

Please download and install the latest LTS version of [node.js](https://nodejs.org/en/download/) for most compatibility. 

```bash
npm install
```

## Usage

```bash
npm start
```

## Test
```bash
npm run test
```

## Author
Yuanpeng Zheng

## License
[MIT](https://choosealicense.com/licenses/mit/)